import asyncio
import pyppeteer

from requests_html import HTMLSession


class BrowserPreloadedHTMLSession(HTMLSession):
    """
    Customized HTMLSession. Allows to run a preloaded Chromium browser under
    a specific user/group.
    """

    def __init__(self, mock_browser=True):
        super(BrowserPreloadedHTMLSession, self).__init__()

    @property
    def browser(self):
        if not hasattr(self, "_browser"):
            self.loop = asyncio.get_event_loop()
            self._browser = self.loop.run_until_complete(
                pyppeteer.launch(
                    headless=True,
                    args=['--no-sandbox'],
                    executablePath='/usr/bin/google-chrome-stable'
                )
            )
        return self._browser
