import { Component, Input } from '@angular/core';

import { AnalyzerResult } from '../analyzer-result';

@Component({
selector: 'app-analyzer-output',
  templateUrl: './analyzer-output.component.html',
  styleUrls: ['./analyzer-output.component.css']
})
export class AnalyzerOutputComponent {

  @Input() analyzerResult: AnalyzerResult;

  constructor() { }

}
