from urllib.parse import urlparse

import requests

from .types import HeadingsSummary, LinksSummary


def html_version_by_doctype(doctype):
    for part in doctype.split('//'):
        if part.lower().startswith('dtd '):
            return part[4:]
    if doctype.lower() == '<!doctype html>':
        return 'HTML 5'
    return None


def retrieve_doctype(html):
    return html.lxml.getroottree().docinfo.doctype


def retrieve_title(html):
    return html.find('title', first=True).text


def count_headings(html):
    summary = HeadingsSummary()
    for heading_level in range(1, 7):
        heading_tag = f'h{heading_level}'
        setattr(summary, heading_tag, len(html.find(heading_tag)))
    return summary


def is_inaccessible(url):
    try:
        response = requests.get(url)
    except requests.RequestException:
        return False
    return response.ok


def count_links(html):
    parsed_url = urlparse(html.url)
    scheme = parsed_url.scheme
    host = f'{parsed_url.scheme}://{parsed_url.netloc}'

    summary = LinksSummary()

    for link in html.links:
        if link.startswith('//'):
            link_full_url = f'{scheme}{link}'
            summary.external_num += 1
        elif link.startswith('/'):
            link_full_url = f'{host}{link}'
            summary.internal_num += 1
        elif link.startswith('http'):
            link_full_url = link
            summary.external_num += 1
        else:
            link_full_url = f'{host}/{link}'
            summary.internal_num += 1

        if not is_inaccessible(link_full_url):
            summary.inaccessible_num += 1

    return summary


def find_login_form(html):
    """
    Check if the page contains a login form.
    We assume that any form with "login" or "signin" button suits our needs.
    """

    valid_login_button_names = ['login', 'log in', 'signin', 'sign in']

    for form in html.find('form'):
        for button in form.find('button'):
            button_text = button.text.lower()
            if any(name in button_text for name in valid_login_button_names):
                return True

        for submit_input in form.find('input[type=submit]'):
            submit_text = submit_input.attrs.get('value').lower()
            if any(name in submit_text for name in valid_login_button_names):
                return True

    return False
