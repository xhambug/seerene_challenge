import { Component, EventEmitter, Output } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { AnalyzerApiService } from '../analyzer-api.service';
import { AnalyzerResult } from '../analyzer-result';

@Component({
  selector: 'app-analyzer-form',
  templateUrl: './analyzer-form.component.html',
  styleUrls: ['./analyzer-form.component.css']
})
export class AnalyzerFormComponent {

  url = '';
  error = '';
  loading = false;

  @Output() analyzerResultReceived = new EventEmitter<AnalyzerResult>();
  @Output() analyzerResultRequested = new EventEmitter<void>();

  constructor(private analyzerApiService: AnalyzerApiService) { }

  requestAnalysis(): void {
    this.analyzerResultRequested.emit();
    this.analyzerApiService.requestAnalysis(this.url)
      .subscribe(
        response => {
          this.loading = false;
          this.analyzerResultReceived.emit(response);
        },
        error => {
          this.error = `Failed to request Analyzer API: ${error.statusText}`;
        }
      );
  }

  onSubmit() {
    this.error = '';
    this.loading = true;
    this.requestAnalysis();
  }
}
