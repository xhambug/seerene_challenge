import { Component } from '@angular/core';

import { AnalyzerResult } from './analyzer-result';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  analyzerResult: AnalyzerResult;

  analyzerResultReceived(analyzerResult: AnalyzerResult) {
    this.analyzerResult = analyzerResult;
  }

  analyzerResultRequested() {
    this.analyzerResult = null;
  }
}
