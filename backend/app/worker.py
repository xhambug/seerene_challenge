import os

import redis
from rq import Connection, Worker

from config import REDIS_QUEUES


def runworker():
    redis_url = os.environ['REDIS_URL']
    redis_connection = redis.from_url(redis_url)
    with Connection(redis_connection):
        worker = Worker(REDIS_QUEUES)
        worker.work()


if __name__ == '__main__':
    runworker()
