import { Injectable } from '@angular/core';
import { LowerCasePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs/Observable';
import { map, take } from 'rxjs/operators';
import { interval } from 'rxjs/observable/interval';
import { _throw } from 'rxjs/observable/throw';
import { of } from 'rxjs/observable/of';

import { AnalyzerResult } from './analyzer-result';

import { environment } from '../environments/environment';


class AnalyzerResponse {
  job_id: string;
}

class AnalyzerStatusResponse {
  result: AnalyzerResult;
  status: string;
  message: string;
}


@Injectable()
export class AnalyzerApiService {

  private analyzerTriggerEndpoint = `${environment.apiUrl}/analyze`;
  private pollingEndpoint = `${environment.apiUrl}/status`;

  constructor(private http: HttpClient) { }


  requestAnalysis(siteUrl): Observable<AnalyzerResult> {
    const pollInterval = 1000;
    const maxAttempts = 600;

    return new Observable(observer => {
      this.http.post<AnalyzerResponse>(this.analyzerTriggerEndpoint, {'url': siteUrl})
        .subscribe(triggerResponse => {
          const jobId = triggerResponse.job_id;
          const pollingUrl = `${this.pollingEndpoint}/${jobId}`;
          const polling = interval(pollInterval).pipe(take(maxAttempts));
          const subscriber = polling.subscribe(
            () => {
              this.http.get<AnalyzerStatusResponse>(pollingUrl).subscribe(
                response => {
                  if (response.status === 'finished') {
                    subscriber.unsubscribe();
                    observer.next(response.result);
                  }
                  if (response.status === 'failed') {
                    subscriber.unsubscribe();
                    observer.error({statusText: response.message});
                  }
                },
                error => {
                  subscriber.unsubscribe();
                  observer.error(error);
                }
              );
            },
            error => { observer.error(error); },
            () => { observer.error({statusText: 'Reached timeout: 10 minutes.'}); },
          );
        });
    });
  }
}
