from requests import RequestException

from .html_session import BrowserPreloadedHTMLSession
from .types import FetchingError, PageSummary
from .utils import (
    count_headings,
    count_links,
    find_login_form,
    html_version_by_doctype,
    retrieve_doctype,
    retrieve_title,
)


session = BrowserPreloadedHTMLSession()


def analyze_html_page(html):
    html.render()

    doctype = retrieve_doctype(html)
    html_version = html_version_by_doctype(doctype)
    title = retrieve_title(html)
    headings_summary = count_headings(html)
    links_summary = count_links(html)
    has_login_form = find_login_form(html)

    return PageSummary(
        doctype=doctype,
        html_version=html_version,
        title=title,
        headings_summary=headings_summary,
        links_summary=links_summary,
        has_login_form=has_login_form,
    )


def analyze_by_url(url):
    """
    Fetch webpage by the the given url and return an error if site fetching
    faild or a analysis summary, if it succeeded
    """

    try:
        response = session.get(url)
    except RequestException as err:
        return None, FetchingError(str(err))

    if not response.ok:
        return None, FetchingError(response.reason, code=response.status_code)

    return analyze_html_page(response.html), None


def analyze_by_url_task(url):
    summary, error = analyze_by_url(url)
    if error:
        return {
            'ok': False,
            'error': error.as_camelcase_dict(),
            'summary': None,
        }
    return {
        'ok': True,
        'error': None,
        'summary': summary.as_camelcase_dict(),
    }
