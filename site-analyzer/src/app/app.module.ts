import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { AnalyzerFormComponent } from './analyzer-form/analyzer-form.component';
import { AnalyzerApiService } from './analyzer-api.service';
import { AnalyzerOutputComponent } from './analyzer-output/analyzer-output.component';


@NgModule({
  declarations: [
    AppComponent,
    AnalyzerFormComponent,
    AnalyzerOutputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [AnalyzerApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
