class FetchingError():
    def __init__(self, message, code=None):
        self.message = message
        self.code = code

    def as_camelcase_dict(self):
        return self.__dict__


class HeadingsSummary():
    def __init__(self, h1=0, h2=0, h3=0, h4=0, h5=0, h6=0):
        self.h1 = h1
        self.h2 = h2
        self.h3 = h3
        self.h4 = h4
        self.h5 = h5
        self.h6 = h6

    def as_camelcase_dict(self):
        return self.__dict__


class LinksSummary():
    def __init__(self, internal_num=0, external_num=0, inaccessible_num=0):
        self.internal_num = internal_num
        self.external_num = external_num
        self.inaccessible_num = inaccessible_num

    def as_camelcase_dict(self):
        return {
            'internalNum': self.internal_num,
            'externalNum': self.external_num,
            'inaccessibleNum': self.inaccessible_num,
        }


class PageSummary():
    def __init__(
        self, doctype, html_version, title, headings_summary, links_summary, has_login_form
    ):
        self.doctype = doctype
        self.html_version = html_version
        self.title = title
        self.headings_summary = headings_summary
        self.links_summary = links_summary
        self.has_login_form = has_login_form

    def as_camelcase_dict(self):
        return {
            'doctype': self.doctype,
            'htmlVersion': self.html_version,
            'title': self.title,
            'headingsSummary': self.headings_summary.as_camelcase_dict(),
            'linksSummary': self.links_summary.as_camelcase_dict(),
            'hasLoginForm': self.has_login_form,
        }
