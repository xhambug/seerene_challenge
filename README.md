# Seerene challenge solution

## Requirements

- Docker

## Run locally

To run locally just call `docker-compose up`.

Once the build is up and running you can reach the app at http://localhost:4200
