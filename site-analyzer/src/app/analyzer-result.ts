class HeadingsSummary {
h1 = 0;
  h2 = 0;
  h3 = 0;
  h4 = 0;
  h5 = 0;
  h6 = 0;
}

class LinksSummary {
  internalNum: number;
  externalNum: number;
  inaccessibleNum: number;
}

class SiteSymmary {
  htmlVersion: string;
  doctype: string;
  title: string;
  headingsSummary: HeadingsSummary;
  linksSummary: LinksSummary;
  hasLoginForm: boolean;
}

class SiteFetchingError {
  code: number;
  message: string;
}

export class AnalyzerResult {
  ok: boolean;
  error: SiteFetchingError;
  summary: SiteSymmary;
}
