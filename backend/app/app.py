import os

import redis
from flask import Flask, g, request
from flask_cors import cross_origin
from flask_json import FlaskJSON, JsonError, as_json
from rq import push_connection, pop_connection, Queue

from analyzer import analyze_by_url_task


app = Flask(__name__, root_path='')
FlaskJSON(app)
app.config['JSON_ADD_STATUS'] = False


@app.route('/api/analyze', methods=['POST'])
@cross_origin()
@as_json
def analyze_api():
    site_url = request.json.get('url')
    if not site_url:
        raise JsonError(status_=400, description='"url" param required')

    task_timeout = 900  # 15 min
    q = Queue()
    job = q.enqueue(analyze_by_url_task, site_url, timeout=task_timeout)
    return {'job_id': job.get_id()}


@app.route('/api/status/<job_id>')
@cross_origin()
@as_json
def job_status(job_id):
    q = Queue()
    job = q.fetch_job(job_id)
    if job is None:
        return {'status': 'unknown'}

    response = {
        'status': job.get_status(),
        'result': job.result,
    }
    if job.is_failed:
        response['message'] = job.exc_info.strip().split('\n')[-1]
    return response


def get_redis_connection():
    redis_connection = getattr(g, '_redis_connection', None)
    if redis_connection is None:
        redis_url = os.environ['REDIS_URL']
        redis_connection = g._redis_connection = redis.from_url(redis_url)
    return redis_connection


@app.before_request
def push_rq_connection():
    push_connection(get_redis_connection())


@app.teardown_request
def pop_rq_connection(exception=None):
    pop_connection()
